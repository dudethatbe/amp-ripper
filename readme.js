console.log(
  'Hello, here is the two-step guide for downloading module files:\n\
 1. Go to amp.dascene.net and look up the Composer you are looking for. Go to their page containing all of their Modules. Right-click page and Save As "amp.html" here\n\
 2. Then run "npm start" to download every module from the Composer\n'
);
