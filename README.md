# amp-ripper

---

## how do you use it?

```
git clone ...
cd ampripper
npm install
```

In your browser, navigate to a listing of module files (search "4mat", click on artist, then click on Modules)
Right-click page to Save As "amp.html" inside this folder

```
npm start
Found 15 Modules
[1/15] downloaded MOD....
```

This will download and unzip the files directly here where the repo folder is. It's up to you to clean-up and move the files afterwards

----

## why did you do this?
Please respect the owners and maintainers of the Amiga Music Preservation site. I understand that it may not be feasible on their end to download a bunch of files at once; There's so much on offer there that it may not be a priority. I can live with that, and I can also automate the process of clicking a bunch of times. Not to mention the repetitive proces of renaming the files to work on a modern OS

## what does it do exactly?
This reads an HTML file, finds all of the links to download module files, downloads them, fixes up the filenames, and unpacks them. After each file is uncompressed, a waiting process will take over to reduce the demand on their site.

