const fs = require("fs/promises");
const path = require("path");
const cheerio = require("cheerio");
const axios = require("axios");
const regex = /[^a-zA-Z0-9.]/g;
const { promisify } = require("util");
const { gunzip } = require("zlib");
const shuffle = require("shuffle-array");
const Gunzip = promisify(gunzip);
const sanitizer = /[^a-zA-Z0-9\-_ ]/;
const package = require("./package.json");
const userAgent = `${package.name} ${package.version} ${package.url}`;
console.log(`starting ${userAgent} ...`);
const getModule = async (endpoint, saveAs) => {
  let url;
  if (/^downmod/.test(endpoint)) {
    url = `http://amp.dascene.net/${endpoint}`;
  } else if (/^http/.test(endpoint)) {
    url = endpoint;
  } else {
    throw Error("unexpected endpoint " + endpoint);
  }
  return axios({
    method: "GET",
    url: url,
    responseType: "stream",
    headers: {
      "User-Agent": userAgent,
    },
  }).then(async (response) => {
    if (response.status !== 200) {
      console.log(response);

      throw new Error(response.statusText);
    }
    const savePath = response.request.path;
    const saveFile = decodeURI(path.basename(savePath));
    const sanitized = saveFile.trim().replace(regex, "_");
    await fs.writeFile(sanitized, response.data);
    return sanitized;
  });
};
const unzip = async (filename, title, composer) => {
  const fileExt = /^([^\.]+\.)/.exec(filename);
  const ext = fileExt[0];
  if (!ext) throw Error("Unable to detect the extension for " + filename);

  // Change the Amiga style file extension to work on modern OS
  // EX: MOD.Pump_up_the_volume => Pump_up_the_volume.MOD
  const safeExt = ext.replace(/(\.)$/, "");
  const sanitizedTitle = title.replace(sanitizer, "");
  const sanitizedComposer = composer.replace(sanitizer, "");

  const newName = `${sanitizedComposer} - ${sanitizedTitle}.${safeExt}`;

  const fileBuffer = await fs.readFile(filename);
  const contents = await Gunzip(fileBuffer);

  await fs.writeFile(newName, contents, "binary");
  return;
};
const backoff = async () => {
  const min = 10;
  const max = 15;
  const timeout = Math.floor(Math.random() * (max - min + 1) + min) * 1000;
  return new Promise((res) => {
    console.log(`waiting ${timeout}ms ...`);
    setTimeout(res, timeout);
  });
};

// start of script
(async () => {
  // change this file path to read a different html file
  const inputFile = "./amp.html";

  // why do i read the file instead of taking the url as input?
  // well if the script doesn't work, then it's easier to look at the input (HTML)
  const buffer = await fs.readFile("./amp.html");
  const $ = cheerio.load(buffer);

  // select all of the downloadable modules; shuffle them to make it less obvious a script is requesting them
  const downloads = $("a[href*='downmod.php?index=']").toArray();
  const shuffled = shuffle(downloads);

  // comment this line to download all; uncomment to limit number of downloads
  // let tests = 1;

  // ok maybe don't edit too many things below
  const testing = typeof tests !== "undefined";
  const total = testing ? tests : shuffled.length;
  let n = 0;
  console.log(`Downloading ${total} file(s)...`);

  for (const element of shuffled) {
    const moduleTitle = element.children[0].data;
    let next = element.parent.next;
    if (next.data === "\n      \n      ") {
      // composer page contains an extra element next to the module title
      next = element.parent.next.next;
    } else {
      // search page does not have the additional empty line after the title
      next = element.parent.next;
    }
    const moduleComposer = next.children[0].children[0].data;
    const moduleUrl = element.attribs.href;

    // download module from "moduleUrl"
    const filename = await getModule(moduleUrl);
    console.log(`[${n}/${total}] downloaded ${filename}`);
    n++;

    // uncompress .gz file in same location
    await unzip(filename, moduleTitle, moduleComposer);

    if (testing) {
      tests--;
      if (tests <= 0) break;
    }

    // wait for a bit before making another request
    await backoff();
  }
  console.log("finished");
})();
